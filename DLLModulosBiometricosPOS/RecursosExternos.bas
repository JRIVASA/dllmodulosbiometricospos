Attribute VB_Name = "RecursosExternos"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpstring _
As Any, ByVal lpFileName As String) As Long

Public Type SystemTime
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type
 
Private Declare Sub GetSystemTime Lib "Kernel32.dll" (lpSystemTime As SystemTime)
Private Declare Sub GetLocalTime Lib "Kernel32.dll" (lpSystemTime As SystemTime)
Private Declare Function GetTickCount Lib "kernel32" () As Long

Global Setup As String

Private Type Conexiones
    VAD10Red As Object
    VAD20Red As Object
    VAD10Local As Object
    VAD20Local As Object
End Type

Public NoExiste As Object

Public Entorno As Conexiones

Public Function sGetIni(sIniFile As String, sSection As String, sKey _
As String, sDefault As String) As String
    Dim sTemp As String * 10000
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, 10000, sIniFile)
    sGetIni = Left$(sTemp, nLength)
End Function
        
Public Function WriteIni(sIniFile As String, sSection As String, sKey _
As String, sValue As String) As String
    
    Dim sTemp As String
    Dim N As Integer

    sTemp = sValue
    
    N = WritePrivateProfileString(sSection, sKey, sValue, sIniFile)
    
End Function

Public Sub Open_Rec(Rec_Local As Boolean, Reg As ADODB.Recordset)

    If Reg.State = adStateOpen Then Reg.Close
    
    If Rec_Local Then
        Reg.CursorLocation = adUseClient
    Else
        Reg.CursorLocation = adUseServer
    End If
    
End Sub

Public Sub Close_Rec(Reg As ADODB.Recordset)
    If Reg.State = adStateOpen Then Reg.Close
    Set Reg = Nothing
End Sub

Public Function Stellar_Mensaje(Mensaje As Integer, Optional pDevolver As Boolean = False) As String
    
    Dim PermiteMultiIdioma As Boolean
    
    PermiteMultiIdioma = Val(sGetIni(Setup, "POS", "PermitirMultiIdioma", 0)) = 1
    
    'uno = activo
    
    If PermiteMultiIdioma Then
        Texto = LoadResString(Mensaje)
    Else
        Select Case Mensaje
            Case 0
                Texto = "Una instancia del programa ya se encuentra en ejecuci�n."
            Case 1
                Texto = "El archivo de configuraci�n no se encontro o esta corrupto."
            Case 2
                Texto = "El servidor local no se encuentra configurado."
            Case 3
                Texto = "El servidor remoto no se encuentra configurado."
            Case 4
                Texto = "El puerto del sevidor SQL no esta configurado."
            Case 5
                Texto = "El valor del puerto del servidor SQl debe ser num�rico."
            Case 6
                Texto = "La sucursal no se encuentra configurada"
            Case 7
                Texto = "La caja no se encuentra configurada."
            Case 8
                Texto = "El consecutivo no se encuentra configurado."
            Case 9
                Texto = "El consecutivo debe ser num�rico."
            Case 10
                Texto = "Se ha producido un error debe Reiniciar el Equipo."
            Case 11
                Texto = "No posee el nivel de seguridad adecuado para este evento."
            Case 12
                Texto = "No esta permitido accesar la caja."
            Case 13
                Texto = "La clave no pertenece a ning�n usuario."
            Case 14
                Texto = "La caja se encuentra asignada a otro usuario."
            Case 15
                Texto = "La cantidad a pagar es mayor. Por favor Verifique."
            Case 16
                Texto = "C�digo no valido. "
            Case 17
                Texto = "No existen compras en espera."
            Case 18
                Texto = "No se puede cargar una compra en espera cuando procesa una."
            Case 19
                Texto = "Compra en espera descartada."
            Case 20
                Texto = "No existen devoluciones ."
            Case 21
                Texto = "La factura no existe."
            Case 23
                Texto = "No existen devoluciones ."
            Case 24
                Texto = "Los datos deben ser num�ricos."
            Case 25
                Texto = "El producto no existe en la factura."
            Case 26
                Texto = "El l�mite debe ser superior al monto facturado."
            Case 27
                Texto = "No existen Bancos  en el sistema."
            Case 28
                Texto = "No existen Monedas en el sistema."
            Case 30
                Texto = "Debe escribir un dato representativo."
            Case 31
                Texto = "Contrase�a incorrecta."
            Case 32
                Texto = "Se llego al l�mite de reintegro para este producto."
            Case 33
                Texto = "El producto no existe en la compra."
            Case 34
                Texto = "El c�digo no existe en el inventario."
            Case 35
                Texto = "El Monto de la Compra ha sido Totalmente Cancelado."
            Case 36
                Texto = "La descripci�n del producto debe ser m�s clara."
            Case 37
                Texto = "No existe el cliente."
            Case 39
                Texto = "El producto no es de balanza."
            Case 40
                Texto = "C�digo no valido. "
            Case 42
                Texto = "Usuario no existe en el sistema."
            Case 43
                Texto = "Existen compras en espera."
            Case 44
                Texto = "No es posible cerrar la caja."
            Case 45
                Texto = "Existen operaciones en progreso."
            Case 46
                Texto = "Usted no ha procesado ninguna venta y no existen compras en espera."
            Case 47
                Texto = "No se puede suspender la caja."
            Case 48
                Texto = "La tarjeta esta vencida."
            Case 49
                Texto = "La fecha de vencimiento no es valida."
            Case 50
                Texto = "No existen manejadores de etiquetas creados en el sistema."
            Case 51
                Texto = "No existen sub-botones definidos para este bot�n."
            Case 52
                Texto = "La llave no fue colocada en la posici�n correcta."
            Case 53
                Texto = "Falla de Conexion con el Servidor, No se puede Realizar la operacion desde el pos"
            Case 54
                Texto = "Esta Operando en Modo [%VAR%] Requiere Autorizaci�n, Informe Esta Anormalidad"
            Case 55
                Texto = "AVISO"
            Case 56
                Texto = "Esta Operando en Modo [%VAR%] Informe Esta Anormalidad"
            Case 57
                Texto = "Ha ocurrido un error en los turnos."
            Case 58
                Texto = "Buscando Servidor [%VAR%]"
            Case 59
                Texto = "No se pudo cerrar el equipo vuelva a intentarlo."
            Case 60
                Texto = "Registro solicitado no puede ser modificado"
            Case 61
                Texto = "Usted ha ganado con la compra N� [%VAR%]"
            Case 62
                Texto = "El precio de venta est� en CERO (0)"
            Case 99
                Texto = "Comuniquese con el Dpto. de Inform�tica"
            Case 101
                Texto = "&Aceptar"
            Case 102
                Texto = "&Cancelar"
            Case 103
                Texto = "C&ontrase�a"
            Case 104
                Texto = "F2 Buscar"
            Case 105
                Texto = "F3  L�mite"
            Case 106
                Texto = "F4 Cantidad"
            Case 107
                Texto = "F5 Balanza"
            Case 108
                Texto = "F6 Reintegrar"
            Case 109
                Texto = "F7 Espera"
            Case 110
                Texto = "F8 Totalizar"
            Case 111
                Texto = "F9 Suspender"
            Case 112
                Texto = "F10 Devolver"
            Case 113
                Texto = "F11 Precios"
            Case 114
                Texto = "F12 Clientes"
            Case 115
                Texto = "Alt-C &Cierre"
            Case 116
                Texto = "Eliminar"
            Case 117
                Texto = "Grabar"
            Case 118
                Texto = "Consultar"
            Case 119
                Texto = "Salir"
            Case 120
                Texto = "Ayuda"
            Case 121
                Texto = "C&argar"
            Case 122
                Texto = "C&olocar"
            Case 123
                Texto = "&Total"
            Case 124
                Texto = "&Parcial"
            Case 125
                Texto = "&Pesado"
            Case 126
                Texto = "&Etiquetado"
            Case 201
                Texto = "Nombre de usuario:"
            Case 202
                Texto = "Contrase�a Usuario:"
            Case 203
                Texto = "Contrase�a Supervisor:"
            Case 204
                Texto = "Caja No."
            Case 205
                Texto = "Versi�n "
            Case 206
                Texto = "Punto de Venta"
            Case 207
                Texto = "Factura No."
            Case 208
                Texto = "Cliente:"
            Case 209
                Texto = "Subtotal"
            Case 210
                Texto = "Impuesto"
            Case 211
                Texto = "Total"
            Case 212
                Texto = "C�digo"
            Case 213
                Texto = "Descripci�n"
            Case 214
                Texto = "Cant."
            Case 215
                Texto = "Precio"
            Case 216
                Texto = "Cancelado"
            Case 217
                Texto = "Cambio"
            Case 218
                Texto = "Compras en espera..!"
            Case 219
                Texto = "Caja"
            Case 220
                'Texto = "R.I.F."
                Texto = PosRetail.SiglasRegistroFiscal
            Case 221
                Texto = "Presione {Esc} para continuar."
            Case 222
                Texto = "Modelo:"
            Case 223
                Texto = "Marca:"
            Case 224
                Texto = "DESDE:"
            Case 225
                Texto = "HASTA:"
            Case 226
                Texto = "Precio de Venta"
            Case 227
                Texto = "Precio Oferta"
            Case 228
                Texto = "FOTOGRAFIA"
            Case 229
                Texto = "DATOS MONETARIOS"
            Case 230
                Texto = "FUERA DE LINEA"
            Case 231
                Texto = "EN LINEA"
            Case 232
                Texto = "ACCESO AL SISTEMA"
            Case 233
                Texto = "Verificando el Servidor"
            Case 234
                Texto = "Actualizando Datos, Por Favor Espere"
            Case 235
                Texto = "Descuento"
            Case 236
                Texto = "Login del Supervisor:"
            Case 237
                Texto = "Total"
            Case 250
                Texto = "No se pudo reconocer su perfil de huella o el mismo no se encuentra registrado en el dispositivo."
            Case 251
                Texto = "Este producto ha sido bloqueado para Venta. Solicite informaci�n al Dpto. de Compras."
            Case 718
                Texto = "El Monto Cancelado Excede el Limite del Cambio"
            Case 719
                Texto = "La Gaveta esta Abierta"
            Case 720
                Texto = "El Producto no puede ser Vendido por Esta Caja!"
            Case 721
                Texto = "Sobrepasa el Limite de Cantidad para Este Producto!"
        End Select
    End If

    If Not pDevolver Then
        NoExiste.Label1.Caption = Texto
        NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If

End Function

'Funciones b�sicas para trabajar con Collections

Public Function Collection_AddKey(pCollection As Collection, pValor, pKey As String) As Boolean
    
    On Error GoTo Err
    
    pCollection.Add pValor, pKey
    
    Collection_AddKey = True
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_AddKey = False
    
End Function

'Public Function Collection_IgualA(pCollection As Collection, pValor) As Boolean
'
'    On Error GoTo Err
'
'    Dim i As Long
'
'    For i = 1 To pCollection.Count
'        If pCollection.Item(i) = pValor Then Collection_IgualA = True: Exit Function
'    Next i
'
'    Exit Function
'
'Err:
'
'    Debug.Print Err.Description
'
'    Collection_IgualA = False
'
'End Function

Public Function Collection_EncontrarValor(pCollection As Collection, pValor, Optional pIndiceStart As Long = 1) As Long
    
    On Error GoTo Err
    
    Dim i As Long
    
    For i = pIndiceStart To pCollection.Count
        If pCollection.Item(i) = pValor Then Collection_EncontrarValor = i: Exit Function
    Next i
    
    Collection_EncontrarValor = -1
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_EncontrarValor = -1
    
End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Function Collection_ExisteIndex(pCollection As Collection, pIndex As Long, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pIndex)
    Else
        Set tmpValorObj = pCollection.Item(pIndex)
    End If
     
    Collection_ExisteIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteIndex = False
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveKey = False
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveIndex = False
    
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function HexToString(ByVal HexToStr As String) As String
    
    Dim strTemp   As String
    Dim StrReturn As String
    Dim i         As Long
    
    For i = 1 To Len(HexToStr) Step 2
        strTemp = Chr$(Val("&H" & Mid$(HexToStr, i, 2)))
        StrReturn = StrReturn & strTemp
    Next i
    
    HexToString = StrReturn
    
End Function

Public Function StringToHex(ByVal StrToHex As String) As String
    
    Dim strTemp   As String
    Dim StrReturn As String
    Dim i         As Long
    
    For i = 1 To Len(StrToHex)
        strTemp = Hex$(Asc(Mid$(StrToHex, i, 1)))
        If Len(strTemp) = 1 Then strTemp = "0" & strTemp
        StrReturn = StrReturn & Space$(1) & strTemp
    Next i
    
    StringToHex = StrReturn
    
End Function

Public Function ReadFileIntoString(StrFilePath As String) As String
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.OpenTextFile(StrFilePath)
    If Not TStream.AtEndOfStream Then ReadFileIntoString = TStream.ReadAll

    Exit Function
    
ErrFile:
    
    ReadFileIntoString = ""
    
End Function

Public Function WriteStringIntoFile(StrFilePath As String, pText As String) As Boolean
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.CreateTextFile(StrFilePath, True, False)
    TStream.Write (pText)
    TStream.Close
    
    WriteStringIntoFile = True

    Exit Function
    
ErrFile:
    
    WriteStringIntoFile = False
    
End Function


' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, _
Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

'Funci�n que devuelve un n�mero entero aleatorio.

Public Function RandomInt(minimo As Long, Maximo As Long) As Long
    Randomize ' inicializar la semilla
    RandomInt = CLng((minimo - Maximo) * Rnd + Maximo)
End Function

Public Sub EsperaManual(ByVal pTiempoSegundos As Long)
    
    If pTiempoSegundos > 0 Then
        
        Dim A As Variant, B As Variant
        
        A = Now
        B = DateAdd("s", pTiempoSegundos, A)
        
        While A < B
            
            A = Now
            DoEvents
            
        Wend
        
    Else
        Exit Sub
    End If
    
End Sub

Public Function GetCurrentSystemTime() As SystemTime
    Dim SysT As SystemTime
    On Error Resume Next
    GetLocalTime SysT
    GetCurrentSystemTime = SysT
End Function

Public Sub EsperaManualMillis(ByVal pTiempoMilliSegundos As Long)
    
    If pTiempoMilliSegundos > 0 Then
        
        Dim mSumar As Variant, mInicio As Variant, CantCiclos As Variant
        
        'Dim A As SystemTime, B As SystemTime
        
        'A = GetCurrentSystemTime
        
        Dim c As Variant, D As Variant
        
        'C = CDec(A.wMilliseconds) + (CDec(A.wSecond) * CDec(1000)) + _
        (CDec(A.wMinute) * CDec(60000)) + (CDec(A.wHour) * CDec(3600000))
        
        c = SafeTickCount
        
        mInicio = c
        
        D = mInicio + pTiempoMilliSegundos
        
        CantCiclos = CDec(0)
        
        Do While c < D
            
            CantCiclos = CantCiclos + 1
            
            'B = GetCurrentSystemTime
            
            'C = CDec(B.wMilliseconds) + (CDec(B.wSecond) * CDec(1000)) + _
            (CDec(B.wMinute) * CDec(60000)) + (CDec(B.wHour) * CDec(3600000))
            
            c = SafeTickCount
            
            DoEvents
            
            If c = 0 Then
                Exit Do
            End If
            
        Loop
        
    Else
        Exit Sub
    End If
    
End Sub

Public Function SafeTickCount() As Long
    On Error Resume Next
    SafeTickCount = GetTickCount
End Function

Public Function GetTickDifInMillis(ByVal pTickIni As Long, ByVal pTickEnd As Long, _
Optional ByVal pIgnoreSeconds As Boolean = True, _
Optional ByRef pFixStartDate As Date, _
Optional ByRef pFixEndDate As Date) As Long
    Dim FullTickDifInMillis As Long
    FullTickDifInMillis = (pTickEnd - pTickIni)
    GetTickDifInMillis = FullTickDifInMillis
    If pIgnoreSeconds Then
        GetTickDifInMillis = GetTickDifInMillis - (Fix(CDec(GetTickDifInMillis) / CDec(1000)) * CDec(1000))
    End If
    If CDec(pFixStartDate) > 0 And CDec(pFixEndDate) Then
        If DateDiff("s", pFixStartDate, pFixEndDate) > Fix(CDec(FullTickDifInMillis) / CDec(1000)) Then
            pFixEndDate = DateAdd("s", -1, pFixEndDate)
        End If
    End If
End Function
