VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MainClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Inicializado As Boolean

Public Sub Inicializar(pConexionADM As Object, pConexionPOS As Object, _
pConexionADMLocal As Object, pConexionPOSLocal As Object, _
pSetup As String)
    
    Setup = pSetup
    
    Set Entorno.VAD10Red = pConexionADM
    Set Entorno.VAD20Red = pConexionPOS
    Set Entorno.VAD10Local = pConexionADMLocal
    Set Entorno.VAD20Local = pConexionPOSLocal
    
    POS_CaptaHuellas_Maneja = Val(sGetIni(Setup, "CAPTAHUELLAS", "Maneja", "0")) = 1
    
    POS_CaptaHuellas_IDDispositivo = sGetIni(Setup, "CAPTAHUELLAS", "Dispositivo", Empty)
    
    'POS_CaptaHuellas_DemoKey = sGetIni(Setup, "CAPTAHUELLAS", "DemoToken", Empty)
    
    POS_CaptaHuellas_TiempoEsperaPOSTVerificacion = Val(sGetIni(Setup, _
    "CAPTAHUELLAS", "TiempoEsperaPOSTVerificacion", "1250"))
    
    'AQUI SOLO RECOJO VALORES DE CONFIGURACION
    'PERO ANTES DE MOSTRAR EL LOGIN VERIFICO QUE EL DISPOSITIVO ESTA EN LAS TABLAS
    'DE ADM LOCAL Y QUE SEA COMPATIBLE CON EL STELLAR
    'SINO SE CUMPLEN ESTAS CONDICIONES ENTONCES NO SE TRABAJA CON BIOMETRICO.
    
    Inicializado = True
    
End Sub

Public Sub InicializarRacionamientoBiometrico()
    
    'ModRacionamientoBiometrico.BioRac_Maneja = Val(sgetini(Setup, "Racionalizacion", "ManejaBiometrico", "0")) = "1"
    'La verificaci�n de si maneja se har� de manera din�mica cada vez que se va a verificar tomando en cuanta varios datos incluyendo la variable del setup
    'debido a que necesita poder volver a reaccionar en caso de que las cajas entren fuera de l�nea y posteriormente se rehabilite la conexi�n con el servidor.
    
    Dim TmpArr As Variant
    
    ModRacionamientoBiometrico.BioRac_IDDispositivo = sGetIni(Setup, "Racionalizacion", "DispositivoBiometrico", "")
    ModRacionamientoBiometrico.BioRac_CalidadMinima = ValidarNumeroIntervalo(Val(sGetIni(Setup, "Racionalizacion", "CalidadMinimaBiometrico", "50")), 100, 1)
    ModRacionamientoBiometrico.BioRac_FormatoHuella = ValidarNumeroIntervalo(sGetIni(Setup, "Racionalizacion", "FormatoHuella", "2"), 2, 0)
    'ModRacionamientoBiometrico.BioRac_NivelAutorizacion = Val(sGetIni(Setup, "Racionalizacion", "Biometrico_NivelAutorizacion", "9"))
    
    'TmpArr = sGetIni(Setup, "Racionalizacion", "Biometrico_ListaAutorizacion", "")
    'TmpArr = Split(TmpArr, "|")
    
    'If Not IsEmpty(TmpArr) Then
        'For Each UsuarioTmp In TmpArr
            'Collection_AddKey ModRacionamientoBiometrico.BioRac_ListaUsuariosAutorizantes, UsuarioTmp, CStr(UsuarioTmp)
        'Next
    'End If
    
End Sub

Public Sub VerificarConfiguracionModuloAcceso()
    Modulo_CaptaHuellas.VerificarConfiguracion
End Sub

Property Get Setup() As String
    Setup = RecursosExternos.Setup
End Property

Property Let Setup(pVal As String)
    RecursosExternos.Setup = pVal
End Property

Property Get POS_CaptaHuellas_Maneja() As Boolean
    POS_CaptaHuellas_Maneja = Modulo_CaptaHuellas.POS_CaptaHuellas_Maneja
End Property

Property Let POS_CaptaHuellas_Maneja(pVal As Boolean)
    Modulo_CaptaHuellas.POS_CaptaHuellas_Maneja = pVal
End Property

Property Get POS_CaptaHuellas_DispositivoCompatible() As Boolean
    POS_CaptaHuellas_DispositivoCompatible = Modulo_CaptaHuellas.POS_CaptaHuellas_DispositivoCompatible
End Property

Property Get ADM_CaptaHuellas_IDDispositivo() As String
    ADM_CaptaHuellas_IDDispositivo = ModuloCaptahuellas.ADM_CaptaHuellas_IDDispositivo
End Property

Property Let POS_CaptaHuellas_DispositivoCompatible(pVal As Boolean)
    Modulo_CaptaHuellas.POS_CaptaHuellas_DispositivoCompatible = pVal
End Property

Property Let POS_CaptaHuellas_IDDispositivo(pVal As String)
    Modulo_CaptaHuellas.POS_CaptaHuellas_IDDispositivo = pVal
End Property

Property Let POS_CaptaHuellas_ModeloDispositivo(pVal As String)
    Modulo_CaptaHuellas.POS_CaptaHuellas_ModeloDispositivo = pVal
End Property

Property Let POS_CaptaHuellas_nVerificaciones(pVal As Integer)
    Modulo_CaptaHuellas.POS_CaptaHuellas_nVerificaciones = pVal
End Property

Property Let POS_CaptaHuellas_nMaxErrores(pVal As Integer)
    Modulo_CaptaHuellas.POS_CaptaHuellas_nMaxErrores = pVal
End Property

Property Get POS_CaptaHuellas_LocalidadUsuarios() As String
    POS_CaptaHuellas_LocalidadUsuarios = Modulo_CaptaHuellas.POS_CaptaHuellas_LocalidadUsuarios
End Property

Property Let POS_CaptaHuellas_LocalidadUsuarios(pVal As String)
    Modulo_CaptaHuellas.POS_CaptaHuellas_LocalidadUsuarios = pVal
End Property

Property Let POS_CaptaHuellas_CriterioSQL(pVal As String)
    Modulo_CaptaHuellas.POS_CaptaHuellas_CriterioSQL = pVal
End Property

Public Function ValidarAccesoCaptaHuellas() As Boolean
    ValidarAccesoCaptaHuellas = Modulo_CaptaHuellas.ValidarAccesoCaptaHuellas
End Function

Property Get ValidacionEnProceso() As Boolean
    ValidacionEnProceso = Modulo_CaptaHuellas.ValidacionEnProceso
End Property

Public Function VerificarUsuario(ByRef pUsuario As String, ByRef pAplicaPos As Boolean, _
ByRef pNivelUsuario As Integer, ByRef pUsuarioDevolucion As String, _
ByRef pDescripcion As String, ByRef pAceptado As Boolean) As Boolean
    VerificarUsuario = Modulo_CaptaHuellas.VerificarUsuario(pUsuario, pAplicaPos, _
    pNivelUsuario, pUsuarioDevolucion, _
    pDescripcion, pAceptado)
End Function

Property Get POS_CaptaHuellas_CodUsuarioActivo() As String
    POS_CaptaHuellas_CodUsuarioActivo = Modulo_CaptaHuellas.POS_CaptaHuellas_CodUsuarioActivo
End Property

Property Let FrmMensajesPOS(pVal As Object)
    Set RecursosExternos.NoExiste = pVal
End Property

Property Get BioRac_Maneja(pClsRac As Object, pConectado As Boolean) As Boolean
    BioRac_Maneja = ModRacionamientoBiometrico.BioRac_Maneja(pClsRac, pConectado)
End Property

Property Get BioRac_DispositivoCompatible() As Boolean
    BioRac_DispositivoCompatible = ModRacionamientoBiometrico.BioRac_DispositivoCompatible
End Property

Property Let BioRac_CriterioSQL(pVal As String)
    ModRacionamientoBiometrico.BioRac_CriterioSQL = pVal
End Property

Property Get BioRac_CodClienteActivo() As String
    BioRac_CodClienteActivo = ModRacionamientoBiometrico.BioRac_CodClienteActivo
End Property

Property Get BioRac_ClienteValidado() As Boolean
    BioRac_ClienteValidado = ModRacionamientoBiometrico.BioRac_ClienteValidado
End Property

Property Let BioRac_ClienteValidado(pVal As Boolean)
    ModRacionamientoBiometrico.BioRac_ClienteValidado = pVal
End Property

'Property Get BioRac_NivelAutorizacion() As Integer
    'BioRac_NivelAutorizacion = ModRacionamientoBiometrico.BioRac_NivelAutorizacion
'End Property

'Property Get BioRac_ListaUsuariosAutorizantes() As Collection
    'Set BioRac_ListaUsuariosAutorizantes = ModRacionamientoBiometrico.BioRac_ListaUsuariosAutorizantes
'End Property

'Property Get BioRac_ListaIdentidadesAutorizadas() As Collection
    'Set BioRac_ListaIdentidadesAutorizadas = ModRacionamientoBiometrico.BioRac_ListaIdentidadesAutorizadas
'End Property

'Property Let BioRac_ListaIdentidadesAutorizadas(pVal As Collection)
    'Set ModRacionamientoBiometrico.BioRac_ListaIdentidadesAutorizadas = pVal
'End Property

Public Function BioRac_ValidarAccesoCaptaHuellas() As Boolean
    BioRac_ValidarAccesoCaptaHuellas = ModRacionamientoBiometrico.BioRac_ValidarAccesoCaptaHuellas
End Function

Public Function LeerStatusCompra(pCorrelativo As String, pConectadoLinea As Boolean, _
pClsRac As Object, pRif As String) As Boolean
    LeerStatusCompra = ModRacionamientoBiometrico.LeerStatusCompra( _
    pCorrelativo, pConectadoLinea, pClsRac, pRif)
End Function

Public Function GrabarStatusCompra(pCorrelativo As String, _
pAdmiteProductosDeRacionamiento As Boolean, pFechaCompra As Date) As Boolean
    GrabarStatusCompra = ModRacionamientoBiometrico.GrabarStatusCompra(pCorrelativo, _
    pAdmiteProductosDeRacionamiento, pFechaCompra)
End Function
