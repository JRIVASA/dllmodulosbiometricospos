Attribute VB_Name = "Modulo_CaptaHuellas"
Global POS_CaptaHuellas_Maneja                          As Boolean
Global POS_CaptaHuellas_IDDispositivo                   As String
Global POS_CaptaHuellas_ModeloDispositivo               As String
Global POS_CaptaHuellas_nVerificaciones                 As Integer
Global POS_CaptaHuellas_nMaxErrores                     As Integer
Global POS_CaptaHuellas_CriterioSQL                     As String
Global POS_CaptaHuellas_LocalidadUsuarios               As String
Global POS_CaptaHuellas_CodUsuarioActivo                As String
Global POS_CaptaHuellas_DispositivoCompatible           As Boolean
Global POS_CaptaHuellas_DemoKey                         As String

Global POS_CaptaHuellas_TiempoEsperaPOSTVerificacion    As Long

Global ValidacionEnProceso                              As Boolean

Public Sub VerificarConfiguracion()
    
    If POS_CaptaHuellas_Maneja Then
        
        Dim ConfigCaptaHuellas As New ADODB.Recordset
        
        ConfigCaptaHuellas.Open _
        "SELECT * FROM MA_CAPTAHUELLAS_PERFILES " & _
        "WHERE ID_DISPOSITIVO = '" & POS_CaptaHuellas_IDDispositivo & "' ", _
        Entorno.VAD10Local, adOpenStatic, adLockReadOnly
        
        If Not ConfigCaptaHuellas.EOF Then
            
            POS_CaptaHuellas_ModeloDispositivo = ConfigCaptaHuellas!c_MODELO
            POS_CaptaHuellas_nVerificaciones = ConfigCaptaHuellas!nu_NumVerificaciones
            POS_CaptaHuellas_nMaxErrores = ConfigCaptaHuellas!nu_MaxErroresVerificacion
            POS_CaptaHuellas_LocalidadUsuarios = sGetIni(Setup, "CAPTAHUELLAS", "LocalidadUsuarios", "?")
            
        Else
            
            If POS_CaptaHuellas_IDDispositivo = "DEMO" Then
                
                POS_CaptaHuellas_ModeloDispositivo = "DEMO"
                POS_CaptaHuellas_nVerificaciones = 1
                POS_CaptaHuellas_nMaxErrores = 1
                
            Else
                
                POS_CaptaHuellas_Maneja = False
                'POS_CaptaHuellas_DispositivoCompatible = False
                
                MsgBox "No se encontró la configuración del dispositivo CaptaHuellas especificado."
                
            End If
            
        End If
        
        POS_CaptaHuellas_DispositivoCompatible = VerificarDispositivoCompatible
        
        ConfigCaptaHuellas.Close
        
    End If
    
End Sub

Public Function VerificarDispositivoCompatible() As Boolean
    
    Select Case POS_CaptaHuellas_ModeloDispositivo
        
        Case Is = "Digital Persona", "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            
            VerificarDispositivoCompatible = True
            
        Case Is = "BioTrack", "BioTrack BioUsb"
            
            VerificarDispositivoCompatible = True
            
        Case Is = "Suprema", "Suprema BioMini (SFR300)", "Suprema BioMini Slim (SFU-S20)", "Suprema BioMini Plus"
            
            VerificarDispositivoCompatible = True
            
        Case Is = "DEMO"
            
            'Dim TmpValidToken As String
            
            'TmpValidToken = "*" & Mid(Year(Now), 4, 1) & "d" & Right(Format(Month(Now), "00"), 1) & _
            "3M" & StrReverse(Format(Hour(Now), "00")) & "0" & Right(StrReverse(Format(Minute(Now), "00")), 1) & "k" & _
            IIf(Month(Now) >= 1 And Month(Now) <= 3, "A", IIf(Month(Now) >= 4 And Month(Now) <= 6, "B", _
            IIf(Month(Now) >= 7 And Month(Now) <= 9, "C", "D")))
            
            'If Modulo_CaptaHuellas.POS_CaptaHuellas_DemoKey = TmpValidToken Then
                VerificarDispositivoCompatible = True
            'Else
                'VerificarDispositivoCompatible = False
            'End If
            
        Case Else
            
            VerificarDispositivoCompatible = False
            
            MsgBox "Disculpe, en estos momentos no existe compatibilidad para utilizar este modelo de dispositivo Capta Huellas."
            
    End Select
    
End Function

Public Function ValidarAccesoCaptaHuellas() As Boolean
    
    Select Case POS_CaptaHuellas_ModeloDispositivo
        
        Case Is = "Digital Persona", "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasDPUAREUCFPR(POS_CaptaHuellas_CriterioSQL)
            
        Case Is = "BioTrack", "BioTrack BioUsb"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasBioTrack(POS_CaptaHuellas_CriterioSQL)
            
        Case Is = "Suprema", "Suprema BioMini (SFR300)", "Suprema BioMini Slim (SFU-S20)", "Suprema BioMini Plus"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasSuprema(POS_CaptaHuellas_CriterioSQL)
            
        Case Is = "DEMO"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasDEMO(POS_CaptaHuellas_CriterioSQL)
            
        Case Else
            
            ValidarAccesoCaptaHuellas = False
            
    End Select
    
    If Not ValidarAccesoCaptaHuellas Then
        POS_CaptaHuellas_CodUsuarioActivo = Empty
    End If
    
End Function

Private Function ValidarAccesoCaptaHuellasDPUAREUCFPR(Criterio As String) As Boolean
    
    ' Validacion para Modelos Digital Persona U.are.U 4000B/4500/Other Compatible FingerPrint Readers
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Object
    Dim PlantillasUsuario() As String
    
    ReDim Preserve PlantillasHuellas(0) As Object
    ReDim Preserve PlantillasUsuario(0) As String
    
    Dim TempObj As Object 'DPFPTemplate
    
    'Debug.Print POS_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open POS_CaptaHuellas_CriterioSQL, _
    Entorno.VAD10Local, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not mRsValidarHuellas.EOF
        
        ReDim Preserve PlantillasHuellas( _
        IIf(PlantillasHuellas(UBound(PlantillasHuellas)) Is Nothing, _
        0, UBound(PlantillasHuellas) + 1))
        
        'Set TempObj = New DPFPTemplate
        
        Set TempObj = CreateObject("DPFPShrX.DPFPTemplate")
        
        Blob = mRsValidarHuellas.Fields("bin_Data").GetChunk(mRsValidarHuellas.Fields("bin_Data").ActualSize)
        
        Call TempObj.Deserialize(CVar(Blob))
        
        Set PlantillasHuellas(UBound(PlantillasHuellas)) = TempObj
        
        ReDim Preserve PlantillasUsuario(UBound(PlantillasHuellas))
        
        PlantillasUsuario(UBound(PlantillasUsuario)) = mRsValidarHuellas!C_CODUSUARIO
        
        mRsValidarHuellas.MoveNext
        
    Wend
    
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCHDPUAUCFPR.Templates = PlantillasHuellas
    frm_VerificacionCHDPUAUCFPR.Usuarios = PlantillasUsuario
    
    For i = 1 To POS_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCHDPUAUCFPR.Verificado = False
        frm_VerificacionCHDPUAUCFPR.Show vbModal
        
        Aceptado = frm_VerificacionCHDPUAUCFPR.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next i
    
    Set frm_VerificacionCHDPUAUCFPR = Nothing
    
    If nIntentosFallidos >= POS_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasDPUAREUCFPR = False
    Else
        ValidarAccesoCaptaHuellasDPUAREUCFPR = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    Debug.Print Err.Description
    
    ValidarAccesoCaptaHuellasDPUAREUCFPR = False
    
    MsgBox "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte técnico o intente de nuevo."
    
    GoTo Finally
    
End Function

Private Function ValidarAccesoCaptaHuellasBioTrack(Criterio As String) As Boolean
    
    ' Validacion para Modelo BioTrack BioUsb (ZK7500)
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    'Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasUsuarios() As String
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasUsuarios(0) As String
    
    Dim TempVar As Variant
    
    'Debug.Print POS_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open POS_CaptaHuellas_CriterioSQL, _
    Entorno.VAD10Local, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not mRsValidarHuellas.EOF
        
        ReDim Preserve PlantillasHuellas( _
        IIf(PlantillasUsuarios(UBound(PlantillasUsuarios)) = Empty, _
        0, UBound(PlantillasHuellas) + 1))
        
        TempVar = mRsValidarHuellas.Fields("bin_DataArray").Value
        
        PlantillasHuellas(UBound(PlantillasHuellas)) = TempVar
        
        ReDim Preserve PlantillasUsuarios(UBound(PlantillasHuellas))
        
        PlantillasUsuarios(UBound(PlantillasUsuarios)) = mRsValidarHuellas!C_CODUSUARIO
        
        mRsValidarHuellas.MoveNext
        
    Wend
    
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCaptaHuellaBioTrackBioUsb.Templates = PlantillasHuellas
    frm_VerificacionCaptaHuellaBioTrackBioUsb.Usuarios = PlantillasUsuarios
    
    For i = 1 To POS_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCaptaHuellaBioTrackBioUsb.Verificado = False
        frm_VerificacionCaptaHuellaBioTrackBioUsb.Show vbModal
        
        Aceptado = frm_VerificacionCaptaHuellaBioTrackBioUsb.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next i
    
    Set frm_VerificacionCaptaHuellaBioTrackBioUsb = Nothing
    
    If nIntentosFallidos >= POS_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasBioTrack = False
    Else
        ValidarAccesoCaptaHuellasBioTrack = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    Debug.Print Err.Description
    
    ValidarAccesoCaptaHuellasBioTrack = False
    
    MsgBox "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte técnico o intente de nuevo."
    
    GoTo Finally
    
End Function

Private Function ValidarAccesoCaptaHuellasSuprema(Criterio As String) As Boolean
    
    ' Validacion para modelos de la marca Suprema.
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    'Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasUsuarios() As String
    Dim PlantillasTamaños() As Long
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasUsuarios(0) As String
    ReDim Preserve PlantillasTamaños(0) As Long
    
    Dim TempVar As Variant
    
    'Debug.Print POS_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open POS_CaptaHuellas_CriterioSQL, _
    Entorno.VAD10Local, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not mRsValidarHuellas.EOF
        
        ReDim Preserve PlantillasHuellas( _
        IIf(PlantillasUsuarios(UBound(PlantillasUsuarios)) = Empty, _
        0, UBound(PlantillasHuellas) + 1))
        
        TempVar = mRsValidarHuellas.Fields("bin_DataArray").Value
        
        PlantillasHuellas(UBound(PlantillasHuellas)) = TempVar
        
        ReDim Preserve PlantillasUsuarios(UBound(PlantillasHuellas))
        
        PlantillasUsuarios(UBound(PlantillasUsuarios)) = mRsValidarHuellas!C_CODUSUARIO
        
        ReDim Preserve PlantillasTamaños(UBound(PlantillasHuellas))
        
        PlantillasTamaños(UBound(PlantillasTamaños)) = UBound(PlantillasHuellas(UBound(PlantillasHuellas))) + 1
        
        mRsValidarHuellas.MoveNext
        
    Wend
    
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCaptaHuellaSuprema.Templates = PlantillasHuellas
    frm_VerificacionCaptaHuellaSuprema.Usuarios = PlantillasUsuarios
    frm_VerificacionCaptaHuellaSuprema.Tamaños = PlantillasTamaños
    
    For i = 1 To POS_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCaptaHuellaSuprema.Verificado = False
        frm_VerificacionCaptaHuellaSuprema.Show vbModal
        
        Aceptado = frm_VerificacionCaptaHuellaSuprema.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next i
    
    Set frm_VerificacionCaptaHuellaSuprema = Nothing
    
    If nIntentosFallidos >= POS_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasSuprema = False
    Else
        ValidarAccesoCaptaHuellasSuprema = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    ValidarAccesoCaptaHuellasSuprema = False
    
    Debug.Print Err.Description
    
    MsgBox "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte técnico o intente de nuevo."
    
    GoTo Finally
    
End Function

Private Function ValidarAccesoCaptaHuellasDEMO(Criterio As String) As Boolean
    
    ' Validacion DEMO
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    'Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasUsuarios() As String
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasUsuarios(0) As String
    
    Dim TempVar As Variant
    
    'Debug.Print POS_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open POS_CaptaHuellas_CriterioSQL, _
    Entorno.VAD10Local, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    While Not mRsValidarHuellas.EOF
        
        ReDim Preserve PlantillasHuellas( _
        IIf(PlantillasUsuarios(UBound(PlantillasUsuarios)) = Empty, _
        0, UBound(PlantillasHuellas) + 1))
        
        TempVar = mRsValidarHuellas.Fields("bin_DataArray").Value
        
        PlantillasHuellas(UBound(PlantillasHuellas)) = TempVar
        
        ReDim Preserve PlantillasUsuarios(UBound(PlantillasHuellas))
        
        PlantillasUsuarios(UBound(PlantillasUsuarios)) = mRsValidarHuellas!C_CODUSUARIO
        
        mRsValidarHuellas.MoveNext
        
    Wend
    
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCaptaHuellaDEMO.Templates = PlantillasHuellas
    frm_VerificacionCaptaHuellaDEMO.Usuarios = PlantillasUsuarios
    
    For i = 1 To POS_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCaptaHuellaDEMO.Verificado = False
        frm_VerificacionCaptaHuellaDEMO.Show vbModal
        
        Aceptado = frm_VerificacionCaptaHuellaDEMO.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next i
    
    Set frm_VerificacionCaptaHuellaDEMO = Nothing
    
    If nIntentosFallidos >= POS_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasDEMO = False
    Else
        ValidarAccesoCaptaHuellasDEMO = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    Debug.Print Err.Description
    
    ValidarAccesoCaptaHuellasDEMO = False
    
    MsgBox "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte técnico o intente de nuevo."
    
    GoTo Finally
    
End Function

Public Function VerificarUsuario( _
ByRef pUsuario As String, ByRef pAplicaPos As Boolean, _
ByRef pNivelUsuario As Integer, ByRef pUsuarioDevolucion As String, _
ByRef pDescripcion As String, ByRef pAceptado As Boolean) As Boolean
    
    On Error GoTo ErrVerif
    
    VerificarUsuario = False
    
    'Set mConexion = Conexion
    'Set Conexion = Entorno.VAD10Local
    
    Dim rsUsuarios As New ADODB.Recordset
    
    rsUsuarios.Open _
    "SELECT * FROM MA_USUARIOS " & _
    "WHERE CODUSUARIO = '" & POS_CaptaHuellas_CodUsuarioActivo & "' " & _
    IIf(POS_CaptaHuellas_LocalidadUsuarios <> "?", _
    "AND LOCALIDAD = '" & POS_CaptaHuellas_LocalidadUsuarios & "' ", ""), _
    Entorno.VAD10Local, adOpenForwardOnly, adLockReadOnly, adCmdText 'Conexion
    
    If Not rsUsuarios.EOF Then
        
        pUsuario = rsUsuarios!CodUsuario 'TmpUsuarioAutorizacion = rsUsuarios!CodUsuario
        pAplicaPos = rsUsuarios!vendedor 'AplicaPos = rsUsuarios!vendedor
        pNivelUsuario = rsUsuarios!nivel 'Nivel_Usuario = rsUsuarios!nivel
        pUsuarioDevolucion = rsUsuarios!CodUsuario 'PosRetail.usuarioDevolucion = rsUsuarios!CodUsuario
        pDescripcion = rsUsuarios!Descripcion 'Autorizo = rsUsuarios!Descripcion
        
        If pAplicaPos Then
            pAceptado = True
        Else
            pAceptado = False
        End If
        
        VerificarDatosUsuario = pAceptado
        
    Else
        
        pAceptado = False
        Call Stellar_Mensaje(250, False)
        pNivel_Usuario = 0
        pDescripcion = "************" 'Autorizo = "************"
        VerificarUsuario = pAceptado
        
    End If
    
    'Set Conexion = mConexion
    
    Exit Function
    
ErrVerif:
    
    VerificarUsuario = False
    
End Function
