VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{D95CB779-00CB-4B49-97B9-9F0B61CAB3C1}#4.0#0"; "biokey.ocx"
Begin VB.Form frm_VerificacionCaptaHuellaDEMO 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Proceso de Verificación."
   ClientHeight    =   1170
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   3270
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frm_VerificacionCaptaHuellaDEMO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1170
   ScaleWidth      =   3270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ZKFPEngXControl.ZKFPEngX ZKEngine 
      Left            =   2880
      Top             =   120
      EnrollCount     =   3
      SensorIndex     =   0
      Threshold       =   10
      VerTplFileName  =   ""
      RegTplFileName  =   ""
      OneToOneThreshold=   10
      Active          =   0   'False
      IsRegister      =   0   'False
      EnrollIndex     =   0
      SensorSN        =   ""
      FPEngineVersion =   "9"
      ImageWidth      =   0
      ImageHeight     =   0
      SensorCount     =   0
      TemplateLen     =   1152
      EngineValid     =   0   'False
      ForceSecondMatch=   0   'False
      IsReturnNoLic   =   -1  'True
      LowestQuality   =   30
   End
   Begin VB.Timer TimerCapture 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   0
      Top             =   0
   End
   Begin VB.Timer TimerIntentosFallidos 
      Enabled         =   0   'False
      Left            =   1440
      Top             =   -240
   End
   Begin VB.Timer Timer_Barra 
      Interval        =   50
      Left            =   1920
      Top             =   -240
   End
   Begin MSComctlLib.ProgressBar Barra 
      Height          =   300
      Left            =   1440
      TabIndex        =   0
      Top             =   600
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   529
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Timer Timer_EsperaPostVerificacion 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   2400
      Top             =   -240
   End
   Begin VB.Label lbl_TipoVerificacion 
      Alignment       =   2  'Center
      Caption         =   "Usuarios"
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   1560
      TabIndex        =   2
      Top             =   900
      Width           =   1215
   End
   Begin VB.Image PicAccessUnrecognized 
      Height          =   600
      Left            =   480
      Picture         =   "frm_VerificacionCaptaHuellaDEMO.frx":628A
      Stretch         =   -1  'True
      Top             =   0
      Width           =   600
   End
   Begin VB.Image PicAccessWaiting 
      Height          =   600
      Left            =   480
      Picture         =   "frm_VerificacionCaptaHuellaDEMO.frx":7DCC
      Stretch         =   -1  'True
      Top             =   600
      Width           =   600
   End
   Begin VB.Image PicAccessDenied 
      Height          =   600
      Left            =   840
      Picture         =   "frm_VerificacionCaptaHuellaDEMO.frx":7FD5
      Stretch         =   -1  'True
      Top             =   240
      Width           =   600
   End
   Begin VB.Image PicAccessGranted 
      Height          =   600
      Left            =   120
      Picture         =   "frm_VerificacionCaptaHuellaDEMO.frx":8F47
      Top             =   240
      Width           =   600
   End
   Begin VB.Label lbl_Msg 
      Caption         =   "Esperando Comprobación."
      ForeColor       =   &H8000000D&
      Height          =   375
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frm_VerificacionCaptaHuellaDEMO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Templates As Variant
Public Usuarios As Variant
Public Verificado As Boolean
Public SelectedPic As String

Private UniTemplateFull() As Byte
Private UniTemplatePart() As Byte
Private UniTemplateSize As Long
Private Const MaxTemplateSize = 1024

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            If Me.Visible Then Me.Hide
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Cancel = 1
    Call Form_KeyDown(vbKeyF12, 0)
End Sub

Private Sub TimerCapture_Timer()
    TimerCapture.Enabled = False
    Dim TmpNum As Long
    TmpNum = RandomInt(1, 10)
    Select Case TmpNum
        Case Is <= 5
            ZKEngine_OnCapture True, Array(RandomInt(0, 255), RandomInt(0, 255), RandomInt(0, 255), _
            RandomInt(0, 255), RandomInt(0, 255), RandomInt(0, 255))
        Case Else
            ZKEngine_OnCapture False, Empty
    End Select
End Sub

Private Sub TimerIntentosFallidos_Timer()
    SelectedPic = "AccessWaiting": SetPic
End Sub

Private Sub ZKEngine_OnCapture(ByVal ActionResult As Boolean, _
ByVal ATemplate As Variant)
    
    Dim Respuesta As Boolean, TestBool As Boolean, TestObj As Object
    Dim CurrentDataTemplate As Variant
    
    Timer_Barra.Enabled = False
    Barra.Value = 100
    
    Respuesta = ActionResult
    
    Dim TmpUser As Long
    
    TmpUser = RandomInt(0, UBound(Usuarios))
        
    If Respuesta Then
        Verificado = True
        POS_CaptaHuellas_CodUsuarioActivo = Usuarios(TmpUser)
    End If
    
    If Not Respuesta Then
       Verificado = False
       SelectedPic = "AccessDenied": SetPic
    Else
       Verificado = True
       SelectedPic = "AccessGranted": SetPic
    End If
    
    ' Por motivos de seguridad no simulamos una entrada exitosa, ya que esto seria una vulnerabilidad.
    ' Solo demostraremos el Acceso Denegado o Concedido de manera visual cambiando el icono. Eso es todo.
    Respuesta = False
    Verificado = False
    POS_CaptaHuellas_CodUsuarioActivo = Empty
    
    Timer_EsperaPostVerificacion.Enabled = True
    
End Sub

Private Sub Form_Activate()
    
    Timer_EsperaPostVerificacion.Enabled = False
    Timer_EsperaPostVerificacion.Interval = POS_CaptaHuellas_TiempoEsperaPOSTVerificacion
    Barra.Value = 0
    Timer_Barra.Enabled = True
    Verificado = False
    
    SelectedPic = "AccessWaiting": SetPic
    
    TimerCapture.Enabled = False
    TimerCapture.Interval = 2500
    TimerCapture.Enabled = True
    
End Sub

Private Sub Form_Load()
    
    ' Set properties to ZKEngine object.
    
    On Error Resume Next
    
    PicAccessWaiting.Top = PicAccessGranted.Top
    PicAccessUnrecognized.Top = PicAccessGranted.Top
    PicAccessWaiting.Left = 550: PicAccessGranted.Left = 550: PicAccessDenied.Left = 550: PicAccessUnrecognized.Left = 550
    
End Sub

Private Sub Timer_Barra_Timer()
    Barra.Value = IIf((Barra.Value + 4) > 100, 4, Barra.Value + 4)
End Sub

Private Sub Timer_EsperaPostVerificacion_Timer()
    Timer_EsperaPostVerificacion.Enabled = False
    Me.Hide
    Timer_Barra.Enabled = False
End Sub

Private Sub SetPic()
    Select Case SelectedPic
        Case "AccessGranted"
            PicAccessGranted.Visible = True
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessDenied"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = True
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessWaiting"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = True
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessUnrecognized"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = True
            TimerIntentosFallidos.Interval = 1500
            TimerIntentosFallidos.Enabled = True
    End Select
End Sub
