VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frm_VerificacionCaptaHuellaSuprema 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Proceso de Verificación."
   ClientHeight    =   1170
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   3270
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frm_VerificacionCaptaHuellaSuprema.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1170
   ScaleWidth      =   3270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer TimerIntentosFallidos 
      Enabled         =   0   'False
      Left            =   1440
      Top             =   -240
   End
   Begin VB.Timer Timer_Barra 
      Interval        =   50
      Left            =   1920
      Top             =   -240
   End
   Begin MSComctlLib.ProgressBar Barra 
      Height          =   300
      Left            =   1440
      TabIndex        =   0
      Top             =   600
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   529
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Timer Timer_EsperaPostVerificacion 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   2400
      Top             =   -240
   End
   Begin VB.Label lbl_TipoVerificacion 
      Alignment       =   2  'Center
      Caption         =   "Usuarios"
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   1560
      TabIndex        =   2
      Top             =   900
      Width           =   1215
   End
   Begin VB.Image PicAccessUnrecognized 
      Height          =   600
      Left            =   480
      Picture         =   "frm_VerificacionCaptaHuellaSuprema.frx":628A
      Stretch         =   -1  'True
      Top             =   0
      Width           =   600
   End
   Begin VB.Image PicAccessWaiting 
      Height          =   600
      Left            =   480
      Picture         =   "frm_VerificacionCaptaHuellaSuprema.frx":7DCC
      Stretch         =   -1  'True
      Top             =   600
      Width           =   600
   End
   Begin VB.Image PicAccessDenied 
      Height          =   600
      Left            =   840
      Picture         =   "frm_VerificacionCaptaHuellaSuprema.frx":7FD5
      Stretch         =   -1  'True
      Top             =   240
      Width           =   600
   End
   Begin VB.Image PicAccessGranted 
      Height          =   600
      Left            =   120
      Picture         =   "frm_VerificacionCaptaHuellaSuprema.frx":8F47
      Top             =   240
      Width           =   600
   End
   Begin VB.Label lbl_Msg 
      Caption         =   "Esperando Comprobación."
      ForeColor       =   &H8000000D&
      Height          =   375
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frm_VerificacionCaptaHuellaSuprema"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Templates As Variant
Public Usuarios As Variant
Public Tamaños As Variant
Public Verificado As Boolean
Public SelectedPic As String

Private sStat As UFS_STATUS
Private mStat As UFM_STATUS
Private BioScanner As Long
Private BioID As Long
Private Matcher As Long
Private Const MaxTemplateSize = 1024

Private UniTemplateFull() As Byte
Private UniTemplatePart() As Byte
Private UniTemplateSize As Long
Private MinQuality As Long
Private Quality As Long

Private CancelEnroll As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            CancelEnroll = True
            If Me.Visible Then Me.Hide
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Cancel = 1
    Call Form_KeyDown(vbKeyF12, 0)
End Sub

Private Sub TimerIntentosFallidos_Timer()
    SelectedPic = "AccessWaiting": SetPic
End Sub

Private Sub Form_Activate()
    
    Timer_EsperaPostVerificacion.Enabled = False
    Timer_EsperaPostVerificacion.Interval = POS_CaptaHuellas_TiempoEsperaPOSTVerificacion
    Barra.Value = 0
    Timer_Barra.Enabled = True
    Verificado = False
    
    SelectedPic = "AccessWaiting": SetPic
    
    StartCapture
    
End Sub

Private Function GetCurrentScannerHandle(ByRef hScanner As Long) As Boolean
    
    sStat = UFS_GetScannerHandle(0, hScanner)
    
    If (sStat = UFS_STATUS.OK) Then
        GetCurrentScannerHandle = True
        Exit Function
    Else
        GetCurrentScannerHandle = False
        Exit Function
    End If
    
End Function

Private Sub Form_Load()
    
    'Set properties to Suprema objects.
    
    On Error Resume Next
     
    PicAccessWaiting.Top = PicAccessGranted.Top
    PicAccessUnrecognized.Top = PicAccessGranted.Top
    PicAccessWaiting.Left = 550: PicAccessGranted.Left = 550: PicAccessDenied.Left = 550: PicAccessUnrecognized.Left = 550
    
    MinQuality = 60
    
    sStat = UFS_Init
    
    If sStat <> UFS_STATUS.OK Then
        MsgBox "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificación.", vbInformation, "Stellar isPOS."
        Unload Me
        Set frm_VerificacionCaptaHuellaSuprema = Nothing
        Exit Sub
    End If
    
    sStat = UFS_GetScannerNumber(BioID)
    
    If sStat <> UFS_STATUS.OK Then
        MsgBox "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificación.", vbInformation, "Stellar isPOS."
        Unload Me
        Set frm_VerificacionCaptaHuellaSuprema = Nothing
        Exit Sub
    End If
    
    If Not GetCurrentScannerHandle(BioScanner) Then
        MsgBox "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificación.", vbInformation, "Stellar isPOS."
        Unload Me
        Set frm_VerificacionCaptaHuellaSuprema = Nothing
        Exit Sub
    End If
    
    UFS_ClearCaptureImageBuffer (BioScanner)
    
    sStat = UFS_SetParameter(BioScanner, UFS_PARAM.TIMEOUT, 3500)
    
    SetScannerStandard
    
    mStat = UFM_Create(Matcher)
    
    If mStat <> UFM_STATUS.OK Then
        MsgBox "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificación.", vbInformation, "Stellar isPOS."
        Unload Me
        Set frm_VerificacionCaptaHuellaSuprema = Nothing
        Exit Sub
    End If
    
    SetMatcherStandard
    
    mStat = UFM_SetParameter(hMatcher, UFM_PARAM.SECURITY_LEVEL, 6)
    mStat = UFM_SetParameter(hMatcher, UFM_PARAM.FAST_MODE, 1)
    
End Sub

Private Sub StartCapture()
    
    DoEvents
    
    Do While Not CaptureProc
        DoEvents
        If CancelEnroll Then CancelEnroll = False: UFS_Uninit: UFM_Delete (Matcher): Exit Sub
        DoEvents
    Loop
    
    VerificarCliente
    
End Sub

Private Function CaptureProc() As Boolean
    
    On Error GoTo ErrorCapture
    
    CaptureProc = False
    
    SelectedPic = "AccessWaiting": SetPic
    
    sStat = UFS_ClearCaptureImageBuffer(BioScanner)
    
    SetScannerStandard
    
    sStat = UFS_CaptureSingleImage(BioScanner)
    
    If (sStat <> UFS_STATUS.OK) Then
        Exit Function
    End If
    
    ReDim UniTemplateFull(MaxTemplateSize - 1): UniTemplateSize = 0
    
    sStat = UFS_ExtractEx(BioScanner, MaxTemplateSize, UniTemplateFull(0), UniTemplateSize, Quality)
    
    If sStat <> UFS_STATUS.OK Then
        Exit Function
    End If
    
    If Not OnFeatureInfo Then
        Exit Function
    End If
    
    RellenarTemplate
    
    CaptureProc = True
    
    Exit Function
    
ErrorCapture:
    
    CaptureProc = False
    
End Function

Private Sub Timer_Barra_Timer()
    Barra.Value = IIf((Barra.Value + 4) > 100, 4, Barra.Value + 4)
End Sub

Private Sub Timer_EsperaPostVerificacion_Timer()
    Timer_EsperaPostVerificacion.Enabled = False
    Me.Hide
    Timer_Barra.Enabled = False
End Sub

Private Sub SetPic()
    Select Case SelectedPic
        Case "AccessGranted"
            PicAccessGranted.Visible = True
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessDenied"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = True
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessWaiting"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = True
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessUnrecognized"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = True
            TimerIntentosFallidos.Interval = 1500
            TimerIntentosFallidos.Enabled = True
    End Select
End Sub

Private Function OnFeatureInfo() As Boolean
    
    OnFeatureInfo = CBool(Quality >= MinQuality)
    
    If Not OnFeatureInfo Then
        SelectedPic = "AccessUnrecognized": SetPic
    End If
    
End Function

Private Sub SetScannerStandard()
    Select Case 2
        'Case 0
            'sStat = UFS_SetTemplateType(BioScanner, UFS_TEMPLATE_TYPE.UFS_TEMPLATE_TYPE_SUPREMA)
        'Case 1
            'sStat = UFS_SetTemplateType(BioScanner, UFS_TEMPLATE_TYPE.UFS_TEMPLATE_TYPE_ANSI378)
        Case 2
            sStat = UFS_SetTemplateType(BioScanner, UFS_TEMPLATE_TYPE.UFS_TEMPLATE_TYPE_ISO19794_2)
    End Select
End Sub

Private Sub SetMatcherStandard()
    Select Case 2
        'Case 0
            'mStat = UFM_SetTemplateType(Matcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_SUPREMA)
        'Case 1
            'mStat = UFM_SetTemplateType(Matcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_ANSI378)
        Case 2
            mStat = UFM_SetTemplateType(Matcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_ISO19794_2)
    End Select
End Sub

Private Sub RellenarTemplate()
    
    ReDim UniTemplatePart(UniTemplateSize - 1) As Byte
    
    Dim I As Long
    
    For I = 0 To UniTemplateSize - 1
        UniTemplatePart(I) = UniTemplateFull(I)
    Next I
    
End Sub

Private Sub VerificarCliente()
    
    'If ActionResult Then
        
        'Dim TmpArchivoBD As String
        'TmpArchivoBD = App.Path & "\HuellaRegistradaBD.Tpl"
        
        Dim Respuesta As Long, TestBool As Boolean, TestObj As Object
        Dim CurrentDataTemplate() As Byte, CurrentTemplateSize As Long ', Tmp As String
        
        Timer_Barra.Enabled = False
        Barra.Value = 100
        
        ' Compare feature set with all stored templates.
        
        For I = 0 To UBound(Templates)
            
            ' Get template from storage.
            
            CurrentDataTemplate = Templates(I)
            CurrentTemplateSize = Tamaños(I)
            
            mStat = UFM_Verify(Matcher, UniTemplatePart(0), UniTemplateSize, _
            CurrentDataTemplate(0), CurrentTemplateSize, Respuesta)
            
            'Tmp = HexToString(CStr(Templates(i)))
            
            'Call WriteStringIntoFile(TmpArchivoBD, CStr(Templates(i))) 'tmp
            
            'Respuesta = ZKEngine.VerFingerFromStr(CStr(CurrentDataTemplate), CStr(Tmp), False, True)
            'Respuesta = ZKEngine.VerFingerFromFile(TmpArchivoBD, ZKEngine.VerTplFileName, False, True)
            
            'currentDataTemplate = ZKEngine.DecodeTemplate1(currentDataTemplate)
            'Respuesta = ZKEngine.VerFinger(currentDataTemplate, ATemplate, False, True)
            
            'Debug.Print ATemplate
            'Respuesta = ZKEngine.VerFingerFromStr(CStr(currentDataTemplate), CStr(ATemplate), False, False)
            
            ' If match, exit from loop.
            
            If CBool(Respuesta) Then
                Verificado = True
                POS_CaptaHuellas_CodUsuarioActivo = Usuarios(I)
                Exit For
            End If
            
            'End If
            
        Next I
        
        UFS_Uninit
        UFM_Delete (Matcher)
        
        If Not CBool(Respuesta) Then
           Verificado = False
           SelectedPic = "AccessDenied": SetPic
        Else
           Verificado = True
           SelectedPic = "AccessGranted": SetPic
        End If
        
        Timer_EsperaPostVerificacion.Enabled = True
        
    'End If
    
End Sub
