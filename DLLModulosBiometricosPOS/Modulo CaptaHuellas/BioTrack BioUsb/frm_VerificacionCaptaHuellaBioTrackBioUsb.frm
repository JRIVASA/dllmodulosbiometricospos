VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{D95CB779-00CB-4B49-97B9-9F0B61CAB3C1}#4.0#0"; "biokey.ocx"
Begin VB.Form frm_VerificacionCaptaHuellaBioTrackBioUsb 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Proceso de Verificación."
   ClientHeight    =   1170
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   3270
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frm_VerificacionCaptaHuellaBioTrackBioUsb.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1170
   ScaleWidth      =   3270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ZKFPEngXControl.ZKFPEngX ZKEngine 
      Left            =   2880
      Top             =   120
      EnrollCount     =   3
      SensorIndex     =   0
      Threshold       =   10
      VerTplFileName  =   ""
      RegTplFileName  =   ""
      OneToOneThreshold=   10
      Active          =   0   'False
      IsRegister      =   0   'False
      EnrollIndex     =   0
      SensorSN        =   ""
      FPEngineVersion =   "9"
      ImageWidth      =   0
      ImageHeight     =   0
      SensorCount     =   0
      TemplateLen     =   1152
      EngineValid     =   0   'False
      ForceSecondMatch=   0   'False
      IsReturnNoLic   =   -1  'True
      LowestQuality   =   30
      FakeFunOn       =   1
   End
   Begin VB.Timer TimerIntentosFallidos 
      Enabled         =   0   'False
      Left            =   1440
      Top             =   -240
   End
   Begin VB.Timer Timer_Barra 
      Interval        =   50
      Left            =   1920
      Top             =   -240
   End
   Begin MSComctlLib.ProgressBar Barra 
      Height          =   300
      Left            =   1440
      TabIndex        =   0
      Top             =   600
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   529
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Timer Timer_EsperaPostVerificacion 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   2400
      Top             =   -240
   End
   Begin VB.Label lbl_TipoVerificacion 
      Alignment       =   2  'Center
      Caption         =   "Usuarios"
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   1560
      TabIndex        =   2
      Top             =   900
      Width           =   1215
   End
   Begin VB.Image PicAccessUnrecognized 
      Height          =   600
      Left            =   480
      Picture         =   "frm_VerificacionCaptaHuellaBioTrackBioUsb.frx":628A
      Stretch         =   -1  'True
      Top             =   0
      Width           =   600
   End
   Begin VB.Image PicAccessWaiting 
      Height          =   600
      Left            =   480
      Picture         =   "frm_VerificacionCaptaHuellaBioTrackBioUsb.frx":7DCC
      Stretch         =   -1  'True
      Top             =   600
      Width           =   600
   End
   Begin VB.Image PicAccessDenied 
      Height          =   600
      Left            =   840
      Picture         =   "frm_VerificacionCaptaHuellaBioTrackBioUsb.frx":7FD5
      Stretch         =   -1  'True
      Top             =   240
      Width           =   600
   End
   Begin VB.Image PicAccessGranted 
      Height          =   600
      Left            =   120
      Picture         =   "frm_VerificacionCaptaHuellaBioTrackBioUsb.frx":8F47
      Top             =   240
      Width           =   600
   End
   Begin VB.Label lbl_Msg 
      Caption         =   "Esperando Comprobación."
      ForeColor       =   &H8000000D&
      Height          =   375
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frm_VerificacionCaptaHuellaBioTrackBioUsb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Templates As Variant
Public Usuarios As Variant
Public Verificado As Boolean
Public SelectedPic As String

Private UniTemplateFull() As Byte
Private UniTemplatePart() As Byte
Private UniTemplateSize As Long
Private Const MaxTemplateSize = 1024

Private FormaCargada As Boolean
Private ErrorCargando As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            On Error Resume Next
            ZKEngine.EndEngine
            If Me.Visible Then
                Me.Hide
            End If
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Cancel = 1
    Call Form_KeyDown(vbKeyF12, 0)
End Sub

Private Sub TimerIntentosFallidos_Timer()
    SelectedPic = "AccessWaiting": SetPic
End Sub

Private Sub ZKEngine_OnCapture(ByVal ActionResult As Boolean, _
ByVal ATemplate As Variant)
    
    Dim Respuesta As Boolean, TestBool As Boolean, TestObj As Object
    Dim CurrentDataTemplate As Variant
    
    Timer_Barra.Enabled = False
    Barra.Value = 100
    
    ' Compare feature set with all stored templates.
    
    RellenarTemplate
    
    For i = 0 To UBound(Templates)
        
        ' Get template from storage.
        
        CurrentDataTemplate = Templates(i)
        
        Respuesta = ZKEngine.VerFinger(CurrentDataTemplate, UniTemplatePart, False, False)
        
        'Tmp = ZKEngine.GetTemplateAsString
        'Respuesta = ZKEngine.VerFingerFromStr(CStr(currentDataTemplate), CStr(Tmp), False, True)
        
        'currentDataTemplate = ZKEngine.DecodeTemplate1(currentDataTemplate)
        'Respuesta = ZKEngine.VerFinger(currentDataTemplate, ATemplate, False, True)
        
        'Debug.Print ATemplate
        'Respuesta = ZKEngine.VerFingerFromStr(CStr(currentDataTemplate), CStr(ATemplate), False, False)
        
        ' If match, exit from loop.
        
        If Respuesta Then
            Verificado = True
            POS_CaptaHuellas_CodUsuarioActivo = Usuarios(i)
            Exit For
        End If
        
        'End If
        
    Next i
    
    On Error Resume Next
    
    ZKEngine.EndEngine
    
    If Not Respuesta Then
        Verificado = False
        SelectedPic = "AccessDenied": SetPic
    Else
        Verificado = True
        SelectedPic = "AccessGranted": SetPic
    End If
    
    Timer_EsperaPostVerificacion.Enabled = True
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        If ErrorCargando Then
            Unload Me
            Exit Sub
        End If
        
        Timer_EsperaPostVerificacion.Enabled = False
        Timer_EsperaPostVerificacion.Interval = POS_CaptaHuellas_TiempoEsperaPOSTVerificacion
        Barra.Value = 0
        Timer_Barra.Enabled = True
        Verificado = False
        
        SelectedPic = "AccessWaiting": SetPic
        
    End If
    
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    ErrorCargando = False
    
    ' Set properties to ZKEngine object.
    
    On Error GoTo Error
    
    PicAccessWaiting.Top = PicAccessGranted.Top
    PicAccessUnrecognized.Top = PicAccessGranted.Top
    PicAccessWaiting.Left = 550: PicAccessGranted.Left = 550: PicAccessDenied.Left = 550: PicAccessUnrecognized.Left = 550
    
    ZKEngine.LowestQuality = 60
    
    ZKEngine.FPEngineVersion = "10"
    
    Dim RetVal As Long
    
    RetVal = ZKEngine.InitEngine
    
    If RetVal <> 0 Then
        MsgBox True, "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificación."
        'Unload Me
        'Set frm_VerificacionCaptaHuellaBioTrackBioUsb = Nothing
        ErrorCargando = True
        Exit Sub
    End If
    
    Select Case 2
        'Case 0 'Formato Propietario del Captahuellas.
            'No hay que hacer nada, se aplica por defecto.
        Case 1 'Formato Estándar ANSI 378
            If Not CBool(ZKEngine.SetTemplateFormat(0)) Then 'Manejo de Estándares
                'MsgBox "Existe un problema al definir el formato de estándares. Por favor contacte a Soporte Técnico.", vbInformation, "Stellar isPOS."
                'Unload Me
                'Set frm_VerificacionCaptaHuellaBioTrackBioUsb = Nothing
                'Exit Sub
            End If
        Case 2 'Formato Estándar ISO 19794-2
            If Not CBool(ZKEngine.SetTemplateFormat(1)) Then 'Manejo de Estándares
                'MsgBox "Existe un problema al definir el formato de estándares. Por favor contacte a Soporte Técnico.", vbInformation, "Stellar isPOS."
                'Unload Me
                'Set frm_VerificacionCaptaHuellaBioTrackBioUsb = Nothing
                'Exit Sub
            End If
    End Select
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    'IgnorarActivate
    
    MsgBox "Existe un problema con el dispositivo, " & _
    "no se pudo iniciar el motor de verificación. " & _
    mErrorDesc & " (" & mErrorNumber & ")"
    
    'Unload Me
    'Set frm_VerificacionCaptaHuellaBioTrackBioUsb = Nothing
    
    ErrorCargando = True
    
End Sub

Private Sub Timer_Barra_Timer()
    Barra.Value = IIf((Barra.Value + 4) > 100, 4, Barra.Value + 4)
End Sub

Private Sub Timer_EsperaPostVerificacion_Timer()
    Timer_EsperaPostVerificacion.Enabled = False
    Me.Hide
    Timer_Barra.Enabled = False
End Sub

Private Sub SetPic()
    Select Case SelectedPic
        Case "AccessGranted"
            PicAccessGranted.Visible = True
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessDenied"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = True
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessWaiting"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = True
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = False
            TimerIntentosFallidos.Enabled = False
        Case "AccessUnrecognized"
            PicAccessGranted.Visible = False
            PicAccessWaiting.Visible = False
            PicAccessDenied.Visible = False
            PicAccessUnrecognized.Visible = True
            TimerIntentosFallidos.Interval = 1500
            TimerIntentosFallidos.Enabled = True
    End Select
End Sub

Private Sub ZKEngine_OnFeatureInfo(ByVal AQuality As Long)
    Debug.Print "Calidad: " & ZKEngine.LastQuality & ". Intento " & IIf(AQuality <> 0, "Fallido", "Acertado") & ". Prosiga."
    If AQuality <> 0 Then SelectedPic = "AccessUnrecognized": SetPic
End Sub

Private Sub RellenarTemplate()
    
    UniTemplatePart = ZKEngine.GetTemplate
    UniTemplateSize = UBound(UniTemplatePart) + 1
    
    ReDim UniTemplateFull(MaxTemplateSize - 1) As Byte
    
    Dim i As Long
    
    For i = 0 To UniTemplateSize - 1
        UniTemplateFull(i) = UniTemplatePart(i)
    Next i
    
End Sub
