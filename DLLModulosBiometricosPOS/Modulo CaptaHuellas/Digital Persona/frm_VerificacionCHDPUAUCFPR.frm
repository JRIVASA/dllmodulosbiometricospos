VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frm_VerificacionCHDPUAUCFPR 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Proceso de Verificación."
   ClientHeight    =   1170
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   3270
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frm_VerificacionCHDPUAUCFPR.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1170
   ScaleWidth      =   3270
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer_Barra 
      Interval        =   50
      Left            =   1920
      Top             =   -240
   End
   Begin MSComctlLib.ProgressBar Barra 
      Height          =   300
      Left            =   1440
      TabIndex        =   1
      Top             =   600
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   529
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Timer Timer_EsperaPostVerificacion 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   2400
      Top             =   -240
   End
   Begin VB.Label lbl_TipoVerificacion 
      Alignment       =   2  'Center
      Caption         =   "Usuarios"
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   1560
      TabIndex        =   2
      Top             =   900
      Width           =   1215
   End
   Begin VB.Label lbl_Msg 
      Caption         =   "Esperando Comprobación."
      ForeColor       =   &H8000000D&
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frm_VerificacionCHDPUAUCFPR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Templates As Variant
Public Usuarios As Variant
Public Verificado As Boolean

Private WithEvents DPFPVerificationCtrl As VBControlExtender
Attribute DPFPVerificationCtrl.VB_VarHelpID = -1
Private InnerObj As Object

Private FormaCargada As Boolean

Private Sub DPFPVerificationCtrl_ObjectEvent(Info As EventInfo)
    
    With Info.EventParameters
        
        Select Case Info.Name
            Case "OnComplete"
                Call DPFPVerificationCtrl_OnComplete( _
                .Item("pFeatureSet").Value, .Item("pStatus").Value)
            Case Else ' Unknown Event
                ' Handle unknown events here.
        End Select
        
    End With
    
End Sub

Private Sub DPFPVerificationCtrl_OnComplete(ByVal pFeatureSet As Object, ByVal pStatus As Object)
    
    Dim Respuesta As Object
    Dim CurrentDataTemplate As Object 'DPFPTemplate
    Dim Verificador As Object 'DPFPVerification
    
    'Set Verificador = New DPFPVerification
    Set Verificador = CreateObject("DPFPEngX.DPFPVerification")
    
    Timer_Barra.Enabled = False
    Barra.Value = 100
    
    ' Compare feature set with all stored templates.
    
    For I = 0 To UBound(Templates)
        
        ' Get template from storage.
        
        Set CurrentDataTemplate = Templates(I)
        
        If CurrentDataTemplate Is Nothing Then
            
        Else
            
            ' Compare feature set with particular template.
            
            Set Respuesta = Verificador.Verify(pFeatureSet, CurrentDataTemplate)
            
            ' If match, exit from loop.
            
            If Respuesta.Verified = True Then
                Verificado = True
                POS_CaptaHuellas_CodUsuarioActivo = Usuarios(I)
                Exit For
            End If
            
        End If
        
    Next I
    
    Timer_EsperaPostVerificacion.Enabled = True
    
    If Respuesta Is Nothing Then
        pStatus.Status = EventHandlerStatusFailure
        Verificado = False
        Exit Sub
    ElseIf Respuesta.Verified = False Then
        ' If non-match, notify caller.
        pStatus.Status = EventHandlerStatusFailure
        Verificado = False
    Else
        Verificado = True
    End If
    
End Sub

Private Sub Form_Activate()
    
    If FormaCargada Then
        Exit Sub
    End If
    
    FormaCargada = True
    
    Timer_EsperaPostVerificacion.Enabled = False
    Timer_EsperaPostVerificacion.Interval = POS_CaptaHuellas_TiempoEsperaPOSTVerificacion
    Barra.Value = 0
    Timer_Barra.Enabled = True
    Verificado = False
    
    If DPFPVerificationCtrl Is Nothing Then
        MsgBox "El componente RTE de Digital Persona no está correctamente instalado. " & _
        "Por favor reinstale dicho componente antes de poder continuar."
        Unload Me
        Exit Sub
    End If
    
End Sub

Private Sub Form_Load()
    
    On Error Resume Next
    
    Set DPFPVerificationCtrl = Me.Controls.Add("DPFPActX.DPFPVerificationControl.1", _
    "DPFPVerificationCtrl", Me)
    
    If Err.Number <> 0 Then
        Exit Sub
    End If
    
    With DPFPVerificationCtrl
        
        .Active = True
        .CausesValidation = True
        .Left = 240
        .Top = 240
        .Width = 720
        .Height = 705
        .TabStop = False
        .Visible = True
        
    End With
    
    Set InnerObj = DPFPVerificationCtrl.Object
    'InnerObj.ReaderSerialNumber = "{00000000-0000-0000-0000-000000000000}"
    ' Mejor dejar dicho valor como esta por defecto. Esta instrucción hace que se tarde
    ' mas la aplicación durante unos segundos, a la hora de inicializar.
    
    FormaCargada = False
    
End Sub

Private Sub Timer_Barra_Timer()
    Barra.Value = IIf((Barra.Value + 4) > 100, 4, Barra.Value + 4)
End Sub

Private Sub Timer_EsperaPostVerificacion_Timer()
    
    Timer_EsperaPostVerificacion.Enabled = False
    Me.Hide
    Timer_Barra.Enabled = False
    
End Sub
