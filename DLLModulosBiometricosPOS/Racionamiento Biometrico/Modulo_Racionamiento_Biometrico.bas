Attribute VB_Name = "ModRacionamientoBiometrico"
Global BioRac_IDDispositivo                 As String
Global BioRac_ModeloDispositivo             As String
Global BioRac_nVerificaciones               As Integer
Global BioRac_nMaxErrores                   As Integer
Global BioRac_CriterioSQL                   As String
Global BioRac_CodClienteActivo              As String
Global BioRac_DispositivoCompatible         As Boolean
Global BioRac_ExistenTablas                 As Boolean
Global BioRac_ClienteValidado               As Boolean
Global BioRac_Conexion                      As ADODB.Connection
Global BioRac_CalidadMinima                 As Integer
Global BioRac_FormatoHuella                 As Integer '0 Formato Propietario, 1 Est�ndar ANSI 378, 2 Est�ndar ISO 19794-2
'Global BioRac_NivelAutorizacion             As Integer
'Global BioRac_ListaUsuariosAutorizantes     As New Collection
'Global BioRac_ListaIdentidadesAutorizadas   As New Collection

Public Function BioRac_Maneja(pClsRacionamiento As Object, pConectado As Boolean) As Boolean
    
    On Error GoTo Error
    
    BioRac_Maneja = Val(sGetIni(Setup, "Racionalizacion", "ManejaBiometrico", "0")) = "1"
    
    If Not BioRac_Maneja Then Exit Function
    
    Dim TmpConexionLocal As ADODB.Connection
    
    If pConectado Then
        
        Set TmpConexionLocal = pClsRacionamiento.BuscarConexion(Entorno.VAD20Red)
        
        BioRac_Maneja = pClsRacionamiento.ExistenTablasBiometrico(TmpConexionLocal)
        
        Set BioRac_Conexion = TmpConexionLocal
        
        If BioRac_Maneja Then 'Verificar Detalles
            
            Dim mRsPerfilBioRac As New ADODB.Recordset
            
            mRsPerfilBioRac.Open "SELECT * FROM MA_RACIONALIZACION_DISPOSITIVOSBIOMETRICOS WHERE ID_Dispositivo = '" & _
            BioRac_IDDispositivo & "'", BioRac_Conexion, adOpenStatic, adLockReadOnly, adCmdText
            
            If Not mRsPerfilBioRac.EOF Then
                BioRac_ModeloDispositivo = mRsPerfilBioRac!c_MODELO
                BioRac_nVerificaciones = mRsPerfilBioRac!nu_NumVerificaciones
                BioRac_nMaxErrores = mRsPerfilBioRac!nu_MaxErroresVerificacion
                BioRac_DispositivoCompatible = BioRac_VerificarDispositivoCompatible
            Else
                BioRac_DispositivoCompatible = False
                MsgBox "No se encontr� la configuraci�n del dispositivo CaptaHuellas especificado."
            End If
            
            mRsPerfilBioRac.Close
            
        End If 'Verificar Detalles
        
    Else
        
        BioRac_Maneja = False
        
    End If
    
    Exit Function
    
Error:
    
    MsgBox "Ha ocurrido un error al cargar la configuraci�n del CaptaHuellas para el control de productos."
    
    BioRac_Maneja = False
    
End Function

Public Function BioRac_VerificarDispositivoCompatible() As Boolean
    
    Select Case BioRac_ModeloDispositivo
        
        'Case Is = "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            
            'BioRac_VerificarDispositivoCompatible = True
            
        Case Is = "BioTrack BioUsb"
            
            BioRac_VerificarDispositivoCompatible = True
            
        Case Is = "Suprema Device"
            
            BioRac_VerificarDispositivoCompatible = True
            
        Case Else
            
            BioRac_VerificarDispositivoCompatible = False
            
            MsgBox "Disculpe, en estos momentos no existe compatibilidad para utilizar este modelo de dispositivo Capta Huellas."
            
    End Select
    
End Function

Public Function BioRac_ValidarAccesoCaptaHuellas() As Boolean
    
    Select Case BioRac_ModeloDispositivo
        
        'Case Is = "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            
            'BioRac_ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasDPUAREUCFPR(POS_CaptaHuellas_CriterioSQL)
            
        Case Is = "BioTrack BioUsb"
            
            BioRac_ValidarAccesoCaptaHuellas = BioRac_ValidarBioTrack(BioRac_CriterioSQL)
            
        Case Is = "Suprema Device"
            
            BioRac_ValidarAccesoCaptaHuellas = BioRac_ValidarSuprema(BioRac_CriterioSQL)
            
        Case Else
            
            BioRac_ValidarAccesoCaptaHuellas = False
            
    End Select
    
    If Not BioRac_ValidarAccesoCaptaHuellas Then BioRac_CodClienteActivo = ""
    
End Function

Public Function BioRac_ValidarBioTrack(Criterio As String) As Boolean
    
    ' Validacion para Modelo BioTrack BioUsb (ZK7500)
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    'Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasClientes() As String
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasClientes(0) As String
    
    Dim TempVar As Variant
    
    'Debug.Print BioRac_CriterioSQL
    
    mRsValidarHuellas.Open BioRac_CriterioSQL, BioRac_Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If mRsValidarHuellas.EOF Or mRsValidarHuellas.RecordCount = 0 Then
        
        MsgBox "Usted no est� registrado o no posee huellas capturadas en el sistema." _
        & vbNewLine & "Por favor, dir�jase hacia la estaci�n de registro biom�trico.", vbOKOnly, "Stellar isPOS."
        
        BioRac_ValidarBioTrack = False
        
        Exit Function
        
    End If
    
    While Not mRsValidarHuellas.EOF
        
        ReDim Preserve PlantillasHuellas(IIf(PlantillasClientes(UBound(PlantillasClientes)) = "", 0, UBound(PlantillasHuellas) + 1))
        
        TempVar = mRsValidarHuellas.Fields("bin_DataArray").Value
        
        PlantillasHuellas(UBound(PlantillasHuellas)) = TempVar
        
        ReDim Preserve PlantillasClientes(UBound(PlantillasHuellas))
        
        PlantillasClientes(UBound(PlantillasClientes)) = mRsValidarHuellas!c_codcliente
        
        mRsValidarHuellas.MoveNext
        
    Wend
    
    mRsValidarHuellas.Close
    
    frmBioRac_BioTrackBioUsb.Templates = PlantillasHuellas
    frmBioRac_BioTrackBioUsb.Clientes = PlantillasClientes
    
    For i = 1 To BioRac_nVerificaciones
        
        frmBioRac_BioTrackBioUsb.Verificado = False
        frmBioRac_BioTrackBioUsb.Show vbModal
        
        Aceptado = frmBioRac_BioTrackBioUsb.Verificado
        
        Set frmBioRac_BioTrackBioUsb = Nothing
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next i
    
    If nIntentosFallidos >= BioRac_nMaxErrores Then
        BioRac_ValidarBioTrack = False
    Else
        BioRac_ValidarBioTrack = True
    End If
    
    Exit Function
    
ErrorCaptaHuellas:
    
    BioRac_ValidarBioTrack = False
    
    MsgBox "Ha ocurrido un error durante el proceso de verificaci�n." & vbNewLine & _
    "Contacte al departamento de soporte t�cnico o intente de nuevo."
    
    Debug.Print Err.Description
    
End Function

Public Function BioRac_ValidarSuprema(Criterio As String) As Boolean
    
    ' Validacion para Modelos Suprema (BioMini, Etc)
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    'Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasTama�os() As Long
    Dim PlantillasClientes() As String
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasClientes(0) As String
    ReDim Preserve PlantillasTama�os(0) As Long
    
    Dim TempVar As Variant
    
    'Debug.Print BioRac_CriterioSQL
    
    mRsValidarHuellas.Open BioRac_CriterioSQL, BioRac_Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If mRsValidarHuellas.EOF Or mRsValidarHuellas.RecordCount = 0 Then
        
        MsgBox "Usted no est� registrado o no posee huellas capturadas en el sistema." _
        & vbNewLine & "Por favor, dir�jase hacia la estaci�n de registro biom�trico.", vbOKOnly, "Stellar isPOS."
        
        BioRac_ValidarSuprema = False
        
        Exit Function
        
    End If
    
    While Not mRsValidarHuellas.EOF
        
        ReDim Preserve PlantillasHuellas(IIf(PlantillasClientes(UBound(PlantillasClientes)) = "", 0, UBound(PlantillasHuellas) + 1))
        
        TempVar = mRsValidarHuellas.Fields("bin_DataArray").Value
        
        PlantillasHuellas(UBound(PlantillasHuellas)) = TempVar
        
        ReDim Preserve PlantillasClientes(UBound(PlantillasHuellas))
        
        PlantillasClientes(UBound(PlantillasClientes)) = mRsValidarHuellas!c_codcliente
        
        ReDim Preserve PlantillasTama�os(UBound(PlantillasHuellas))
        
        PlantillasTama�os(UBound(PlantillasTama�os)) = mRsValidarHuellas!n_TemplateSize
        
        mRsValidarHuellas.MoveNext
        
    Wend
    
    mRsValidarHuellas.Close
    
    frmBioRac_SupremaDevice.Templates = PlantillasHuellas
    frmBioRac_SupremaDevice.Clientes = PlantillasClientes
    frmBioRac_SupremaDevice.Tama�os = PlantillasTama�os
    
    For i = 1 To BioRac_nVerificaciones
        
        frmBioRac_SupremaDevice.Verificado = False
        frmBioRac_SupremaDevice.Show vbModal
        
        Aceptado = frmBioRac_SupremaDevice.Verificado
        
        Set frmBioRac_SupremaDevice = Nothing
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next i
    
    If nIntentosFallidos >= BioRac_nMaxErrores Then
        BioRac_ValidarSuprema = False
    Else
        BioRac_ValidarSuprema = True
    End If
    
    Exit Function
    
ErrorCaptaHuellas:
    
    BioRac_ValidarSuprema = False
    
    MsgBox "Ha ocurrido un error durante el proceso de verificaci�n." & vbNewLine & _
    "Contacte al departamento de soporte t�cnico o intente de nuevo."
    
    'Debug.Print Err.Description
    
End Function

Public Function GrabarStatusCompra(pCorrelativo As String, _
pAdmiteProductosDeRacionamiento As Boolean, pFechaCompra As Date) As Boolean
    
    On Error GoTo ErrStatus
    
    Dim ValorActual As String, mItems As Variant, mItemsNew As New Collection
    Dim TmpItm As Variant, Found As Boolean
    
    ValorActual = sGetIni(Setup, "Racionalizacion", "EstatusCompraBiometrico", "")
    
    mItems = Split(ValorActual, "|")
    
    If UBound(mItems) >= 0 Then
        If Not mItems(0) = "" Then
            For Each Itm In mItems
                TmpItm = Split(Itm, "!")
            
                If pCorrelativo = CStr(TmpItm(0)) Then
                    Found = True 'Actualizar
                    mItemsNew.Add Split(pCorrelativo & "!" & IIf(pAdmiteProductosDeRacionamiento, 0, 1) & "!" & Format(pFechaCompra, "DD/MM/YYYY hh:mm:ss ampm"), "!"), CStr(pCorrelativo)
                Else
                    'If Abs(DateDiff("D", CDate(TmpItm(2)), Now)) <> 0 Then
                        'Ya pas� un d�a, La verificaci�n expir�.
                    'Else
                        mItemsNew.Add TmpItm, CStr(TmpItm(0))
                    'End If
                End If
            Next
            
            If Not Found Then mItemsNew.Add Split(pCorrelativo & "!" & IIf(pAdmiteProductosDeRacionamiento, 0, 1) & "!" & Format(pFechaCompra, "DD/MM/YYYY hh:mm:ss ampm"), "!"), CStr(pCorrelativo)
        Else
            mItemsNew.Add Split(pCorrelativo & "!" & IIf(pAdmiteProductosDeRacionamiento, 0, 1) & "!" & Format(pFechaCompra, "DD/MM/YYYY hh:mm:ss ampm"), "!"), CStr(pCorrelativo)
        End If
    Else
        mItemsNew.Add Split(pCorrelativo & "!" & IIf(pAdmiteProductosDeRacionamiento, 0, 1) & "!" & Format(pFechaCompra, "DD/MM/YYYY hh:mm:ss ampm"), "!"), CStr(pCorrelativo)
    End If
    
    On Error GoTo ErrDB
    
    Dim mRsTemp As New ADODB.Recordset
    
    mRsTemp.Open "SELECT DISTINCT ID_PADRE FROM MA_TRANSACCION_TEMP", _
    Entorno.VAD20Local, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Dim mItemsFinal As New Collection
    
    While Not mRsTemp.EOF
        
        If Collection_ExisteKey(mItemsNew, CStr(mRsTemp!ID_PADRE)) Then
            mItemsFinal.Add mItemsNew.Item(CStr(mRsTemp!ID_PADRE))
        End If
        
        mRsTemp.MoveNext
        
    Wend
    
    On Error GoTo ErrStatus
    
    WriteIni Setup, "Racionalizacion", "EstatusCompraBiometrico", ArmarCadenaStatus(mItemsFinal)
    
    GrabarStatusCompra = True
    
    Exit Function
    
ErrStatus:
    
    'Debug.Print Err.Description
    
    'Probablemente se arruin� el String... en estos casos es preferible resetear todo... aunque eso implica no recordar
    'si las compras aplican Productos de racionamiento.
    
    GrabarStatusCompra = False
    
    WriteIni Setup, "Racionalizacion", "EstatusCompraBiometrico", ""
    
    Exit Function
    
ErrDB:
    
    Debug.Print Err.Description
    
    'Si fue un error relacionado con la BD no es necesario borrar el String.
    
    GrabarStatusCompra_HiCC = False
    
End Function

Public Function LeerStatusCompra(pCorrelativo As String, pConectadoLinea As Boolean, _
pClsRac As Object, pRif As String) As Boolean
    
    On Error GoTo ErrStatus
    
    Dim ValorActual As String, mItems As Variant, TmpItm As Variant, Validado As Boolean
    
    If pClsRac.EsCompraNormal_SinRegulados(pRif) Then LeerStatusCompra = True: Exit Function
    
    ValorActual = sGetIni(Setup, "Racionalizacion", "EstatusCompraBiometrico", "")
    
    mItems = Split(ValorActual, "|")
    
    If UBound(mItems) >= 0 Then
        If Not mItems(0) = "" Then
            For Each Itm In mItems
                TmpItm = Split(Itm, "!")
                
                If pCorrelativo = CStr(TmpItm(0)) Then
                    
                    'Debug.Print DateDiff("D", CDate(TmpItm(2)), Now)
                    
                    If Abs(DateDiff("D", CDate(TmpItm(2)), Now)) = 0 Then
                        LeerStatusCompra = (Not CBool(TmpItm(1)))
                        Exit Function
                    Else 'La verificaci�n del biom�trico expir�.
                        
                        If Not pConectadoLinea Then LeerStatusCompra = False: Exit Function  'Si no hay l�nea no funciona el racionamiento asi que... dejar comprar.
                        
                        If MsgBox("La verificaci�n de huella para esta compra ha expirado debido a que la misma se hizo en d�as anteriores." & vbNewLine & "�Desea continuar la compra omitiendo los productos supervisados?" & _
                        vbNewLine & "Presione OK para continuar o Cancelar para introducir la huella nuevamente.", vbOKCancel, "Stellar isPOS.") _
                        <> VbMsgBoxResult.vbCancel Then
                            LeerStatusCompra = True
                        Else
                            
                            BioRac_CriterioSQL = "SELECT * FROM" _
                            & vbNewLine & "MA_RACIONALIZACION_CLIENTES CLI INNER JOIN" _
                            & vbNewLine & "MA_RACIONALIZACION_CLIENTES_HUELLAS HUE" _
                            & vbNewLine & "ON CLI.c_Rif = HUE.c_CodCliente" _
                            & vbNewLine & "WHERE Cli.c_Rif = '" & pRif & "'"
                            
                            Validado = BioRac_ValidarAccesoCaptaHuellas
                            
                            Do While (Not Validado)
                                If MsgBox("La verificaci�n de huella para esta compra ha expirado debido a que la misma se hizo en d�as anteriores." & vbNewLine & "�Desea continuar la compra omitiendo los productos supervisados?" & _
                                vbNewLine & "Presione OK para continuar o Cancelar para introducir la huella nuevamente.", vbOKCancel, "Stellar isPOS.") _
                                = VbMsgBoxResult.vbCancel Then
                                    Validado = BioRac_ValidarAccesoCaptaHuellas
                                Else
                                    Validado = False: Exit Do
                                End If
                            Loop
                            
                            LeerStatusCompra = Not Validado
                            
                        End If
                    End If
                    
                End If
            Next
        End If
    End If
    
    Exit Function
    
ErrStatus:
    
    LeerStatusCompra = False
    
End Function

Private Function ArmarCadenaStatus(pItems As Collection)
    
    Dim CadenaRegistro As String, Count As Long, SubCount As Long
    
    For Each Itm In pItems
        CadenaRegistro = ""
        For Each Field In Itm
            SubCount = SubCount + 1
            CadenaRegistro = CadenaRegistro & CStr(Field) & IIf(SubCount = (UBound(Itm) + 1), "", "!")
            Debug.Print CadenaRegistro
        Next
        SubCount = 0
        Count = Count + 1
        ArmarCadenaStatus = ArmarCadenaStatus & CadenaRegistro & IIf(Count = pItems.Count, "", "|")
        Debug.Print ArmarCadenaStatus
    Next
    
End Function
